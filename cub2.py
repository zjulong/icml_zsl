import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import  optim
from torch.autograd import Variable
from torch.utils.data import DataLoader, TensorDataset
import numpy as np
from scipy import io
import argparse, os
from torch.optim.lr_scheduler import StepLR




class RN(nn.Module):

	def __init__(self, c, d):
		super(RN, self).__init__()
		# channel num and objects num
		self.c = c
		self.d = d

		# convert att from 312 to 2048
		self.attnet = nn.Sequential(
			nn.Linear(312, 1200),
			nn.ReLU(inplace=True),
			nn.Linear(1200, c),
			nn.ReLU(inplace=True)
		)


		# the input is self.c with two coordination information, and then combine each
		self.g = nn.Sequential(nn.Linear( self.c * 2 + 2, 256),
		                       nn.ReLU(inplace=True),
		                       nn.Linear(256, 256),
		                       nn.ReLU(inplace=True),
		                       nn.Linear(256, 256),
		                       nn.ReLU(inplace=True),
		                       nn.Linear(256, 256),
		                       nn.ReLU(inplace=True))

		self.f = nn.Sequential(nn.Linear(256, 256),
		                       nn.ReLU(inplace=True),
		                       nn.Linear(256, 256),
		                       nn.Dropout(),
		                       nn.ReLU(inplace=True),
		                       nn.Linear(256, 64),
		                       nn.BatchNorm1d(64),
		                       nn.ReLU(inplace=True),
		                       nn.Linear(64, 1),
		                       nn.Sigmoid())

		# deepmind coord: [(i // 5 - 2) / 2., (i % 5 - 2) / 2.]
		coord = np.array([(i / self.d, j / self.d) for i in range(self.d) for j in range(self.d)])
		self.coord = torch.from_numpy(coord).float().view(d, d, 2).transpose(0, 2).transpose(1,2).contiguous()
		self.coord = self.coord.unsqueeze(0)
		print('self.coord:', self.coord.size(), self.coord)  # [batchsz:1, 2, self.d, self.d]



	def forward(self, x, att):
		"""

		:param x:   [b, 2048, 7, 7]
		:param att: [k, 312]
		:return:
		"""
		batchsz, c, d, d = x.size()
		k = att.size(0)

		# convert att from 312 to 2048
		# [k, 312] => [k, 2048]
		att = self.attnet(att)

		# coord:[1, 2, d, d] => [b, 2, d, d]
		# cat: [b, c+2, d, d]
		x_coord = torch.cat([x, Variable(self.coord.expand(batchsz, 2, d, d).cuda())], dim=1)
		# [b, c+2, d, d] => [b, 1, c+2, d, d] => [b, k, c+2, d, d]
		x_coord = x_coord.unsqueeze(1).expand(batchsz, k, c + 2, d, d)

		# [k, c] => [k, c, 1, 1] => [k, c, d, d]
		att = att.unsqueeze(2).unsqueeze(3).expand(k, c, d, d)
		# => [1, k, c, d, d] => [b, k, c, d, d]
		att = att.unsqueeze(0).expand(batchsz, k, c, d, d)

		# x_coord: [b, k, c+2, d, d]
		# att:     [b, k, c, d, d]
		# => [b, k, 2*c + 2, d, d]
		comb = torch.cat([x_coord, att], dim=2)
		# now comb: [b, k, c, d, d]
		c = 2 * c + 2

		# [b, k, c, d:1, d:2] => [b, k, d:2, d:1, c] => [b, k, d:1, d:2, c]
		comb = comb.transpose(2, 4).transpose(2, 3).contiguous().view(batchsz * k * d * d, c)
		# push to G network
		# [b*k*d*d, c] => [b*k*d*d, -1] => [b, d*d, -1]
		x_f = self.g(comb).view(batchsz * k, d * d, -1)
		# sum over d*d dim
		# => [b*k, -1]
		x_f = x_f.sum(1)
		# push to F network
		# [b*k, -1] => [b,k]
		score = self.f(x_f).view(batchsz, k)

		return score


def compute_accuracy(rn, x, label, label_table, att_table):
	"""

	:param attnet:
	:param rnnet:
	:param x:
	:param label:
	:param label_table:
	:param att_table:
	:return:
	"""
	data = TensorDataset(x, label)
	batchsz = 8
	loader = DataLoader(data, batch_size=batchsz, shuffle=False)
	total_rewards = 0
	k = label_table.size(0)
	label_table = label_table.cpu().numpy()

	for x_b, label_b in loader:

		# udpate real batchsz
		batchsz = x_b.size(0)
		# [b, 2048, 7, 7], [b, 312]
		batch_features = Variable(x_b).cuda().float()
		relations = rn(batch_features, Variable(att_table.float().cuda())).view(batchsz, k)

		re_batch_labels = []
		for l in label_b.numpy():
			index = np.argwhere(label_table == l)
			re_batch_labels.append(index[0][0])
		re_batch_labels = torch.LongTensor(np.array(re_batch_labels))

		_, predict_labels = torch.max(relations.data, 1)

		rewards = [1 if predict_labels[j] == re_batch_labels[j] else 0 for j in range(batchsz)]
		total_rewards += np.sum(rewards)

	test_accuracy = total_rewards / x.size(0)

	return test_accuracy

def main():

	root = '../cub_all/'
	lr = 1e-5

	# feature = mat['features'].T
	print('loading 7x7 features...')
	feature = np.load(os.path.join(root, 'res101_mycaffe_7x7.npy'))
	print('done.', feature.shape)

	mat = io.loadmat(os.path.join(root, 'res101_goodbad.mat'))
	label = mat['labels'].astype(int).squeeze() - 1
	mat = io.loadmat(os.path.join(root, 'att_splits_goodbad.mat'))
	trainval_idx = mat['trainval_loc'].squeeze() - 1
	test_seen_idx = mat['test_seen_loc'].squeeze() - 1
	test_unseen_idx = mat['test_unseen_loc'].squeeze() - 1
	attribute = mat['original_att'].T

	# [7057, 2048, 7, 7]
	# [7057]
	x_train = feature[trainval_idx]
	label_train = label[trainval_idx].astype(np.int32)

	# [2967, 2048, 7, 7]
	# [2967]
	# [1764, 2048, 7, 7]
	# [1767]
	x_test_unseen = feature[test_unseen_idx]
	label_test_unseen = label[test_unseen_idx].astype(np.int32)
	x_test_seen = feature[test_seen_idx]
	label_test_seen = label[test_seen_idx].astype(np.int32)

	# [50]
	# [50, 312]
	unique_label_unseen = np.unique(label_test_unseen) # we don't need keep order here.
	unique_att_unseen = attribute[unique_label_unseen]
	unique_label_unseen = torch.LongTensor(unique_label_unseen)
	unique_att_unseen = torch.FloatTensor(unique_att_unseen)

	# [200, 312]
	attribute = torch.from_numpy(attribute).float()

	# convert all data to tensor
	x_train = torch.FloatTensor(x_train)
	label_train = torch.LongTensor(label_train)
	x_test_unseen = torch.FloatTensor(x_test_unseen)
	label_test_unseen = torch.LongTensor(label_test_unseen)
	x_test_seen = torch.FloatTensor(x_test_seen)
	label_test_seen = torch.LongTensor(label_test_seen)

	train_data = TensorDataset(x_train, label_train)
	rn = RN(2048, 7)
	rn.cuda()

	rn_optim = optim.Adam(rn.parameters(), lr=lr)
	rn_scheduler = StepLR(rn_optim, step_size=30000, gamma=0.5)

	if os.path.exists("./ckpt/zsl_cub_rn.mdl"):
		rn.load_state_dict(torch.load("./ckpt/zsl_cub_rn.mdl"))
		print("load relation network success")

	last_accuracy = 0.0
	train_loader = DataLoader(train_data, batch_size=32, shuffle=True)
	train_loader = iter(train_loader)

	for episode in range(20000000):
		try:
			# [b, 2048], [b]
			x_b, label_b = train_loader.next()
		except StopIteration:
			train_loader = DataLoader(train_data, batch_size=32, shuffle=True)
			train_loader = iter(train_loader)
			# [b, 2048], [b]
			x_b, label_b = train_loader.next()
		# udpate batchsz
		batchsz = x_b.size(0)

		# get the unique label in label_b.
		# can use np.unique as a alternative but np.unique will sort the label by default.
		# torch.unique is supported only from 0.4.0
		label_b_unique = []
		for l in label_b.numpy():
			if l not in label_b_unique:
				label_b_unique.append(l)
		# [k], k<batchsz
		label_b_unique = torch.LongTensor(np.array(label_b_unique))
		# [k, 312]
		att_b_unique = attribute[label_b_unique]
		#
		k = att_b_unique.size(0)

		# [b, 2048, 7, 7]
		batch_features = Variable(x_b).cuda()
		relations = rn(batch_features, Variable(att_b_unique).cuda()).view(batchsz, k)


		# re-build batch_labels according to sample_labels
		label_b_unique = label_b_unique.cpu().numpy()
		re_batch_labels = []
		for l in label_b.numpy():
			index = np.argwhere(label_b_unique == l)
			re_batch_labels.append(index[0][0])
		# [b]
		re_batch_labels = torch.LongTensor(np.array(re_batch_labels))

		# # compute cross entropy loss
		# loss = criteon(relations, re_batch_labels)
		# eculiden loss
		mse = nn.MSELoss().cuda(0)
		one_hot_labels = Variable(torch.zeros(batchsz, k).scatter_(1, re_batch_labels.view(-1, 1), 1)).cuda()
		loss = mse(relations, one_hot_labels)

		# schedule learning rate
		rn_scheduler.step(episode)
		# update
		rn.zero_grad()
		loss.backward()
		rn_optim.step()

		if (episode + 1) % 100 == 0:
			print("episode:", episode + 1, "\tloss", loss.data[0])

		if (episode + 1) % 2000 == 0:
			# test
			print("Testing...")

			zsl_accuracy = compute_accuracy(rn, x_test_unseen, label_test_unseen,
			                                unique_label_unseen, unique_att_unseen)
			gzsl_unseen_accuracy = compute_accuracy(rn, x_test_unseen, label_test_unseen,
			                                        torch.LongTensor(np.arange(200)), attribute)
			gzsl_seen_accuracy = compute_accuracy(rn, x_test_seen, label_test_seen,
			                                      torch.LongTensor(np.arange(200)), attribute)

			H = 2 * gzsl_seen_accuracy * gzsl_unseen_accuracy / (gzsl_unseen_accuracy + gzsl_seen_accuracy)

			print('zsl:', zsl_accuracy)
			print('gzsl: seen=%.4f, unseen=%.4f, h=%.4f' % (gzsl_seen_accuracy, gzsl_unseen_accuracy, H))

			if zsl_accuracy > last_accuracy:
				# save networks
				torch.save(rn.state_dict(), "./ckpt/zsl_cub_rn.mdl")

				print("save networks for episode:", episode)

				last_accuracy = zsl_accuracy


if __name__ == '__main__':
	main()
