import torch
from torch import nn
from torch import optim
from torch.autograd import Variable
import numpy as np
import os
from scipy import io



class RN(nn.Module):

	def __init__(self):
		super(RN, self).__init__()

		self.attnet = nn.Sequential(
			nn.Linear(312, 1200),
			nn.ReLU(inplace=True),
			nn.Linear(1200, 2048),
			nn.ReLU(inplace=True)
		)

		self.g = nn.Sequential(nn.Linear((self.c + 2) * 2, 256),
		                       nn.ReLU(inplace=True),
		                       nn.Linear(256, 256),
		                       nn.ReLU(inplace=True),
		                       nn.Linear(256, 256),
		                       nn.ReLU(inplace=True),
		                       nn.Linear(256, 256),
		                       nn.ReLU(inplace=True))

		self.f = nn.Sequential(nn.Linear(256, 256),
		                       nn.ReLU(inplace=True),
		                       nn.Linear(256, 256),
		                       nn.Dropout(),
		                       nn.ReLU(inplace=True),
		                       nn.Linear(256, 64),
		                       nn.BatchNorm1d(64),
		                       nn.ReLU(inplace=True),
		                       nn.Linear(64, 1),
		                       nn.Sigmoid())


def main():
	root = '../cub_all'

	data = io.loadmat(os.path.join(root, 'res101_goodbad.mat'))
	# [11788]
	label = data['labels'].astype(int).squeeze() - 1
	data = io.loadmat(os.path.join(root, 'att_splits_goodbad.mat'))
	# [7057]
	train_idx = data['trainval_loc'].squeeze() - 1
	# [1764]
	test_seen_idx = data['test_seen_loc'].squeeze() - 1
	# [2967]
	test_unseen_idx = data['test_unseen_loc'].squeeze() - 1
	# [200, 312]
	att = data['original_att'].T
	# [11788, 2048, 7, 7]
	feature = np.load(os.path.join(root, 'res101_mycaffe_7x7.npy'))

	# [7057, 2048, 7, 7], [7057]
	x_train = feature[train_idx]
	label_train = label[train_idx]
	# [1764, 2048, 7, 7], [1764]
	x_test_seen = feature[test_seen_idx]
	label_test_seen = label[test_seen_idx]
	# [2967, 2048, 7, 7], [2967]
	x_test_unseen = feature[test_unseen_idx]
	label_test_unseen = label[test_unseen_idx]

	# train
	x_train = torch.from_numpy(x_train)
	label_train = torch.from_numpy(label_train)
	att = torch.from_numpy(att)
	# test
	x_test_seen = torch.from_numpy(x_test_seen)
	x_test_unseen = torch.from_numpy(x_test_unseen)
	label_test_seen = torch.from_numpy(label_test_seen)
	label_test_unseen = torch.from_numpy(label_test_unseen)



if __name__ == '__main__':
	main()