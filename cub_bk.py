import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import  optim
from torch.autograd import Variable
from torch.utils.data import DataLoader, TensorDataset
import numpy as np
from scipy import io
import argparse, os
from torch.optim.lr_scheduler import StepLR

class AttributeNetwork(nn.Module):

	def __init__(self, input_size, hidden_size, output_size):
		super(AttributeNetwork, self).__init__()
		self.fc1 = nn.Linear(input_size, hidden_size)
		self.fc2 = nn.Linear(hidden_size, output_size)

	def forward(self, x):
		x = F.relu(self.fc1(x))
		x = F.relu(self.fc2(x))

		return x

class FeatureNetwork(nn.Module):

	def __init__(self, input_size, hidden_size, output_size):
		super(FeatureNetwork, self).__init__()
		self.fc1 = nn.Linear(input_size, hidden_size)
		self.fc2 = nn.Linear(hidden_size, output_size)

	def forward(self, x):
		x = F.relu(self.fc1(x))
		x = F.relu(self.fc2(x))

		return x

class RelationNetwork(nn.Module):

	def __init__(self, input_size, hidden_size, ):
		super(RelationNetwork, self).__init__()
		self.fc1 = nn.Linear(input_size, hidden_size)
		self.fc2 = nn.Linear(hidden_size, 1)

	def forward(self, x):
		x = F.relu(self.fc1(x))
		x = F.sigmoid(self.fc2(x))
		return x


def compute_accuracy(attnet, fnet, rnnet, x, label, label_table, att_table):
	"""

	:param attnet:
	:param rnnet:
	:param x:
	:param label:
	:param label_table:
	:param att_table:
	:return:
	"""
	data = TensorDataset(x, label)
	batchsz = 64
	loader = DataLoader(data, batch_size=batchsz, shuffle=False)
	total_rewards = 0
	k = label_table.size(0)
	label_table = label_table.cpu().numpy()

	for x_b, label_b in loader:

		# udpate real batchsz
		batchsz = x_b.size(0)
		# [b, 2048]
		batch_features = fnet(Variable(x_b).cuda().float())
		# [50]
		sample_features = attnet(Variable(att_table).cuda().float())

		sample_features_ext = sample_features.unsqueeze(0).repeat(batchsz, 1, 1)
		batch_features_ext = batch_features.unsqueeze(0).repeat(k, 1, 1)
		batch_features_ext = torch.transpose(batch_features_ext, 0, 1)

		relation_pairs = torch.cat([sample_features_ext, batch_features_ext], 2).view(-1, 4096)
		relations = rnnet(relation_pairs).view(-1, k)

		re_batch_labels = []
		for l in label_b.numpy():
			index = np.argwhere(label_table == l)
			re_batch_labels.append(index[0][0])
		re_batch_labels = torch.LongTensor(np.array(re_batch_labels))

		_, predict_labels = torch.max(relations.data, 1)

		rewards = [1 if predict_labels[j] == re_batch_labels[j] else 0 for j in range(batchsz)]
		total_rewards += np.sum(rewards)

	test_accuracy = total_rewards / x.size(0)

	return test_accuracy

def main():

	root = '../cub_all/'
	lr = 1e-5

	mat = io.loadmat(os.path.join(root, 'res101_goodbad.mat'))
	# feature = mat['features'].T
	print('loading 7x7 features...')
	feature = np.load(os.path.join(root, 'res101_mycaffe_7x7.npy'))
	feature = feature.reshape(11788, 2048*7*7)
	print('done.', feature.shape)
	label = mat['labels'].astype(int).squeeze() - 1
	mat = io.loadmat(os.path.join(root, 'att_splits_goodbad.mat'))
	trainval_idx = mat['trainval_loc'].squeeze() - 1
	test_seen_idx = mat['test_seen_loc'].squeeze() - 1
	test_unseen_idx = mat['test_unseen_loc'].squeeze() - 1
	attribute = mat['original_att'].T

	# [7057, 2048]
	# [7057]
	x_train = feature[trainval_idx]
	label_train = label[trainval_idx].astype(np.int32)

	# [2967, 2048]
	# [2967]
	# [1764, 2048]
	# [1767]
	x_test_unseen = feature[test_unseen_idx]
	label_test_unseen = label[test_unseen_idx].astype(np.int32)
	x_test_seen = feature[test_seen_idx]
	label_test_seen = label[test_seen_idx].astype(np.int32)

	# [50]
	# [50, 312]
	unique_label_unseen = np.unique(label_test_unseen)
	unique_att_unseen = attribute[unique_label_unseen]
	unique_label_unseen = torch.LongTensor(unique_label_unseen)
	unique_att_unseen = torch.FloatTensor(unique_att_unseen)

	# [200, 312]
	attribute = torch.from_numpy(attribute).float()

	# convert all data to tensor
	x_train = torch.FloatTensor(x_train)
	label_train = torch.LongTensor(label_train)
	x_test_unseen = torch.FloatTensor(x_test_unseen)
	label_test_unseen = torch.LongTensor(label_test_unseen)
	x_test_seen = torch.FloatTensor(x_test_seen)
	label_test_seen = torch.LongTensor(label_test_seen)



	train_data = TensorDataset(x_train, label_train)
	attnet = AttributeNetwork(312, 1200, 2048)
	fnet = FeatureNetwork(100352, 4096, 2048)
	rnnet = RelationNetwork(4096, 1200)
	attnet.cuda()
	rnnet.cuda()
	fnet.cuda()

	attnet_optim = optim.Adam(attnet.parameters(), lr=lr, weight_decay=1e-5)
	rnnet_optim = optim.Adam(rnnet.parameters(), lr=lr)
	fnet_optim = optim.Adam(fnet.parameters(), lr=lr)
	attnet_scheduler = StepLR(attnet_optim, step_size=30000, gamma=0.5)
	rnnet_scheduler = StepLR(rnnet_optim, step_size=30000, gamma=0.5)


	last_accuracy = 0.0
	train_loader = DataLoader(train_data, batch_size=32, shuffle=True)
	train_loader = iter(train_loader)

	for episode in range(200000):
		try:
			# [b, 2048], [b]
			x_b, label_b = train_loader.next()
		except StopIteration:
			train_loader = DataLoader(train_data, batch_size=32, shuffle=True)
			train_loader = iter(train_loader)
			# [b, 2048], [b]
			x_b, label_b = train_loader.next()
		# udpate batchsz
		batchsz = x_b.size(0)

		# get the unique label in label_b.
		# can use np.unique as a alternative but np.unique will sort the label by default.
		# torch.unique is supported only from 0.4.0
		label_b_unique = []
		for l in label_b.numpy():
			if l not in label_b_unique:
				label_b_unique.append(l)
		# [k], k<batchsz
		label_b_unique = torch.LongTensor(np.array(label_b_unique))
		# [k, 312]
		att_b_unique = attribute[label_b_unique]
		#
		k = att_b_unique.size(0)

		# [b, 2048, 7, 7] => [b, 2048]
		batch_features = fnet(Variable(x_b).cuda())
		# [k, 2048]
		sample_features = attnet(Variable(att_b_unique).cuda())
		# [1, k, 2048] => [b, k, 2048]
		sample_features_ext = sample_features.unsqueeze(0).repeat(batchsz, 1, 1)
		# [1, b, 2048] => [k, b, 2048] => [b, k, 2048]
		batch_features_ext = batch_features.unsqueeze(0).repeat(k, 1, 1).transpose(0, 1)
		# [b, k, 2048 + 2048] => [b * k , 4096]
		relation_pairs = torch.cat([sample_features_ext, batch_features_ext], 2).view(batchsz * k, 4096)
		# [b*k, 4096] => [b*k, 1] => [b, k]
		relations = rnnet(relation_pairs).view(batchsz, k)

		# re-build batch_labels according to sample_labels
		label_b_unique = label_b_unique.cpu().numpy()
		re_batch_labels = []
		for l in label_b.numpy():
			index = np.argwhere(label_b_unique == l)
			re_batch_labels.append(index[0][0])
		# [b]
		re_batch_labels = torch.LongTensor(np.array(re_batch_labels))

		# # compute cross entropy loss
		# loss = criteon(relations, re_batch_labels)
		# eculiden loss
		mse = nn.MSELoss().cuda(0)
		one_hot_labels = Variable(torch.zeros(batchsz, k).scatter_(1, re_batch_labels.view(-1, 1), 1)).cuda()
		loss = mse(relations, one_hot_labels)

		# schedule learning rate
		attnet_scheduler.step(episode)
		rnnet_scheduler.step(episode)
		# update
		attnet.zero_grad()
		rnnet.zero_grad()
		fnet.zero_grad()
		loss.backward()
		attnet_optim.step()
		rnnet_optim.step()
		fnet_optim.step()

		if (episode + 1) % 200 == 0:
			print("episode:", episode + 1, "\tloss", loss.data[0])

		if (episode + 1) % 2000 == 0:
			# test
			print("Testing...")

			zsl_accuracy = compute_accuracy(attnet, fnet, rnnet, x_test_unseen, label_test_unseen,
			                                unique_label_unseen, unique_att_unseen)
			gzsl_unseen_accuracy = compute_accuracy(attnet, fnet, rnnet, x_test_unseen, label_test_unseen,
			                                        torch.LongTensor(np.arange(200)), attribute)
			gzsl_seen_accuracy = compute_accuracy(attnet, fnet, rnnet, x_test_seen, label_test_seen,
			                                      torch.LongTensor(np.arange(200)), attribute)

			H = 2 * gzsl_seen_accuracy * gzsl_unseen_accuracy / (gzsl_unseen_accuracy + gzsl_seen_accuracy)

			print('zsl:', zsl_accuracy)
			print('gzsl: seen=%.4f, unseen=%.4f, h=%.4f' % (gzsl_seen_accuracy, gzsl_unseen_accuracy, H))

			if zsl_accuracy > last_accuracy:
				# save networks
				torch.save(attnet.state_dict(), "./ckpt/zsl_cub_attribute_network_v35.mdl")
				torch.save(rnnet.state_dict(), "./ckpt/zsl_cub_relation_network_v35.mdl")

				print("save networks for episode:", episode)

				last_accuracy = zsl_accuracy


if __name__ == '__main__':
	main()
